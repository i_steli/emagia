<?php namespace Core;


class App{
	public static $registries = array();

	public static function bind($key, $registry){
		self::$registries[$key] = $registry;
	}

	public static function get($key){
		if(!array_key_exists($key, self::$registries)){
			throw new \Exception("Key $key is not bind into App");
		}

		return self::$registries[$key];
	}
}