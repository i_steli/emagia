<?php namespace Core;

use phpDocumentor\Reflection\Types\Self_;

class Router{
	public static $routes = [];
	private static $params = [];
	public static function load($routes = '../app/routes/web.php'){
		require $routes;
		return new self;
	}

	public static function checkParams($uri, $method){
		$countParams = preg_match_all("/{([a-zA-z]+)}/", $uri, $mathces);
		$requestUri = request()->uri();

		// check if params exists
		if($countParams == 0) return $uri;
		// check if number of elements between uri and requestUri are the same
		$convertRequestUriArray = explode('/', $requestUri);
		$convertRouteUriArray  = explode( '/', $uri);
		if(count($convertRouteUriArray) !== count($convertRequestUriArray)) return $uri;
		// check if the number of elements between difference of the two uri and number of params are the same
		$arrayDiff = array_diff($convertRequestUriArray, $convertRouteUriArray);
		if($countParams !== count($arrayDiff)) return $uri;

		self::setParmas($arrayDiff);
		return $requestUri;
	}

	public static function setParmas(array $params){
		$params = array_values($params);
		self::$params = $params;
	}

	public static function getParams() : array{
		return self::$params;
	}

	public static function get($uri, $controllerAction){
		$uri = self::checkParams(trim($uri, '/'), 'GET');
		return self::$routes['GET'][$uri] = $controllerAction;
	}

	public static function post($uri, $controllerAction){
		$uri = self::checkParams(trim($uri, '/'), 'POST');
		return self::$routes['POST'][$uri] = $controllerAction;
	}

	public function match($uri, $method){
		//var_dump($uri, $method);
		if(!array_key_exists($method, self::$routes)){
			throw new \Exception("Route method $method not found", 404);
		}

		if(!array_key_exists($uri, self::$routes[$method])){
			throw new \Exception("Route uri $uri not found", 404);
		}

		list($controller, $action) = explode('@', self::$routes[$method][$uri]);

		return $this->callAction($controller, $action);
	}

	public function callAction($controller, $action){
		var_dump(self::$routes, $this->getParams());
		$controller = "App\Controllers\\".$controller;

		return (new $controller)->$action(...$this->getParams());
	}
}