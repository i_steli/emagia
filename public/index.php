<?php
use Core\Router;

require "../vendor/autoload.php";
require "../app/helpers.php";
require "../bootstrap/app.php";

Router::load()->match(request()->uri(), request()->method());

//dd((new Router())::$routes);