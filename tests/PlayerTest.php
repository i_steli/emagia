<?php

use App\Models\Player;
use Core\App;
use Database\Connection;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase{
	private $mockPlayer;
	public static function setUpBeforeClass(): void {
		$configDatabase = require_once "app/config/database.php";
		App::bind('database', $configDatabase);
		App::bind('pdo', Connection::make(App::get('database')['mysql']));
	}

	public function setUp(): void {
		$this->mockPlayer = $this->getMockBuilder( Player::class)
		               ->setMethods(null)
		               ->getMock();
	}

	public function defenderLuckToBlockAttacksProvider(){
		return [
			[50, 1, 20],
			[20, 4, 20],
			[10, 9, 20],
		];
	}

	public function defenderLuckToBlockAttacksEveryTimeProvider(){
		return [
			[1, 1],
			[4, 20],
			[9, 20],
			[10, 50],
			[15, 41],
			[3, 2]
		];
	}

	public function defenderCanNotBlockAttacksProvider(){
		return [
			[50, 2, 20],
			[20, 5, 20],
			[10, 10, 20],
			[0, 10, 20],
		];
	}

	/**
	 * @dataProvider defenderLuckToBlockAttacksProvider
	 */
	public function testDefenderBlockAttacks($luck, $turns, $maxTurns){
		$this->mockPlayer->luck = $luck;
		$response = $this->mockPlayer->isAttackBlocked($this->mockPlayer, $turns, $maxTurns);
		$this->assertTrue($response);
	}

	/**
	 * @dataProvider defenderLuckToBlockAttacksEveryTimeProvider
	 */
	public function testDefenderBlockAttacksEverytime($turns, $maxTurns){
		$this->mockPlayer->luck = 100;
		$response = $this->mockPlayer->isAttackBlocked($this->mockPlayer, $turns, $maxTurns);
		$this->assertTrue($response);
	}

	/**
	 * @dataProvider defenderCanNotBlockAttacksProvider
	 */
	public function testDefenderCanNotBlockAttacks($luck, $turns, $maxTurns){
		$this->mockPlayer->luck = $luck;
		$response = $this->mockPlayer->isAttackBlocked($this->mockPlayer, $turns, $maxTurns);
		$this->assertFalse($response);
	}

	public function testNumberOfUniqueSkills(){
		$player = new Player();
		$player->setSkill('kratos', 10, 20, 'god of war', true, 20);
		$player->setSkill('kratos', 10, 20, 'god of war', true, 20);
		$player->setSkill('kratos', 10, 20, 'god of war', true, 20);

		$this->assertCount(1, $player->getSkills());
	}

	public function testNumberOfSkills(){
		$player = new Player();
		$player->setSkill('kratos', 10, 20, 'god of war 3', true, 20);
		$player->setSkill('kratos', 10, 20, 'god of war 4', true, 20);

		$this->assertCount(2, $player->getSkills());
	}

	public function testDivisionByZeroSkills(){
		$player = new Player();
		$this->expectException(Exception::class);
		$this->expectExceptionMessage('Do not add skill if the chance is 0');
		$player->setSkill('kratos', 0, 20, 'god of war 3', true, 20);
	}

	public function testDamageDoneBySkillsWithDamageAttackerHigherThanDefence(){
		$damage = 20;
		$this->mockPlayer->setSkill('orderus', 50, 20, 'fast attack', true, $damage);
		$this->mockPlayer->setSkill('beast', 50, 20, 'strong defence', false, $damage*0.5);
		$damageArray = $this->mockPlayer->getDamageDoneBySkills('orderus', 'beast', 1, $this->mockPlayer->getSkills(), $damage);

		$this->assertEquals(10, $damageArray['damage_attack'] - $damageArray['damage_def']);
	}

	public function testDamageDoneBySkillsWithincrementDamageAttackerHigherThanDefence(){
		$damage = 20;
		$this->mockPlayer->setSkill('orderus', 50, 20, 'fast attack 1', true, $damage);
		$this->mockPlayer->setSkill('orderus', 50, 20, 'fast attack 2', true, $damage*2);
		$this->mockPlayer->setSkill('beast', 50, 20, 'strong defence', false, $damage*0.5);
		$damageArray = $this->mockPlayer->getDamageDoneBySkills('orderus', 'beast', 1, $this->mockPlayer->getSkills(), $damage);

		$this->assertEquals(50, $damageArray['damage_attack'] - $damageArray['damage_def']);
	}

	public function testDamageDoneBySkillsWithDamageAttackerLowerThanDefence(){
		$damage = 20;
		$this->mockPlayer->setSkill('orderus', 50, 20, 'fast attack', true, $damage);
		$this->mockPlayer->setSkill('beast', 50, 20, 'strong defence', false, $damage*4);
		$damageArray = $this->mockPlayer->getDamageDoneBySkills('orderus', 'beast', 1, $this->mockPlayer->getSkills(), $damage);

		$this->assertEquals(0, $damageArray['damage_attack'] - $damageArray['damage_def']);
	}

	public function testDamageDoneBySkillsWithIncrementDamageDefence(){
		$damage = 20;
		$this->mockPlayer->setSkill('orderus', 50, 20, 'fast attack', true, $damage*3);
		$this->mockPlayer->setSkill('beast', 50, 20, 'strong defence 1', false, $damage);
		$this->mockPlayer->setSkill('beast', 50, 20, 'strong defence 2', false, $damage*2);
		$damageArray = $this->mockPlayer->getDamageDoneBySkills('orderus', 'beast', 1, $this->mockPlayer->getSkills(), $damage);

		$this->assertEquals(0, $damageArray['damage_attack'] - $damageArray['damage_def']);
	}

	public function testDamageDoneBySkillsWithDamageAttackEqualWithDamageDefence(){
		$damage = 20;
		$this->mockPlayer->setSkill('orderus', 50, 20, 'fast attack', true, $damage);
		$this->mockPlayer->setSkill('beast', 50, 20, 'strong defence', false, $damage);
		$damageArray = $this->mockPlayer->getDamageDoneBySkills('orderus', 'beast', 1, $this->mockPlayer->getSkills(), $damage);

		$this->assertEquals(0, $damageArray['damage_attack'] - $damageArray['damage_def']);
	}

}