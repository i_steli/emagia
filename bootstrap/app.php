<?php
session_start();

use App\Config\Game;
use Core\App;
use Database\Connection;

// set database configuration
$configDatabase = require_once "../app/config/database.php";
App::bind('database', $configDatabase);
App::bind('pdo', Connection::make(App::get('database')['mysql']));
// set game configurations
App::bind('game', new Game());
App::get('game')->setMaxTurns(20);
App::get('game')->setPlayerStats('orderus', [
	'health' => rand(70, 100),
	'strength' => rand(70, 80),
	'defence' => rand(45, 55),
	'speed' => rand(40, 50),
	'luck' => rand(10, 30)
]);
App::get('game')->setPlayerStats('beast', [
	'health' => rand(60, 90),
	'strength' => rand(60, 90),
	'defence' => rand(40, 60),
	'speed' => rand(40, 60),
	'luck' => rand(25, 40)
]);