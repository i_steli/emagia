<?php namespace App\Controllers;

use Core\App;

class MigrationController{
	private $pdo;

	public function __construct() {
		$this->pdo = App::get('pdo');
	}

	public function index(){
		$this->players();
		$this->games();
		$this->stats();
		$this->turns();
	}

	public function players() : void{
		$sql = "CREATE TABLE if not exists players(
			    id int(10) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
			    name VARCHAR(256) UNIQUE NOT NULL,
			    active TINYINT DEFAULT FALSE,		
			    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
			)ENGINE=INNODB;";

		$this->pdo->exec($sql);
		$this->pdo->exec("INSERT INTO players (name) VALUES ('orderus'), ('beast')");
	}

	public function games() : void{
		$sql = "CREATE TABLE if not exists games(
			    id int(10) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
			    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
			)ENGINE=INNODB;";

		$this->pdo->exec($sql);
	}

	public function stats() : void{
		$sql = "CREATE TABLE if not exists stats(
			    id int(10) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
			    player_id int(10) UNSIGNED NOT NULL,
			    game_id int(10) UNSIGNED NOT NULL,	
			    health int(10),	
			    strength int(10),
			    defence int(10),
			    speed int(10),
			    luck int(10),
			    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			    CONSTRAINT stats_player_id_fk FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE,
			    CONSTRAINT stats_game_id_fk FOREIGN KEY (game_id) REFERENCES games(id) ON DELETE CASCADE  
			)ENGINE=INNODB;";

		$this->pdo->exec($sql);
	}

	public function turns(){
		$sql = "CREATE TABLE if not exists turns(
			    id int(10) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
			    player_id int(10) UNSIGNED NOT NULL,
			    game_id int(10) UNSIGNED NOT NULL,	
				damage_done int(10),
				health_left int(10),
				hit varchar(256),
				skills TEXT,
			    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			    CONSTRAINT turns_player_id_fk FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE,  
			    CONSTRAINT turns_game_id_fk FOREIGN KEY (game_id) REFERENCES games(id) ON DELETE CASCADE  
			)ENGINE=INNODB;";

		$this->pdo->exec($sql);
	}
}