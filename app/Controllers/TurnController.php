<?php namespace App\Controllers;

use App\Models\Game;
use App\Models\Player;
use App\Models\Turn;
use App\Services\GameService;
use Core\App;
use Exception;

class TurnController{
	public function store(){
		$player = new Player();
		$game = new Game();
		new GameService($game, $player);

		$gameId = $game->getCurrentGame()->id;
		$maxTurns = App::get('game')->getMaxTurns();
		$activePlayer = $player->getActivePlayer();
		$inactivePlayer = $player->getInactivePlayer();
		$damage_done = $activePlayer->strength - $inactivePlayer->defence;
		// check for winner
		if($activePlayer->health <= 0 || $inactivePlayer->health <=0 ){
			header("Location:{$_SERVER['HTTP_ORIGIN']}/start");
			exit;
		}
		// check for turns
		$turn = new Turn();
		$turns = $turn->getTurns($gameId);
		if($turns >= $maxTurns){
			$_SESSION['max_turns'] = 'Game Over Reached the maximum turns';
			header("Location:{$_SERVER['HTTP_ORIGIN']}/start");
			exit;
		}
		// set skillls
		$player->setSkill('orderus', 10, $maxTurns, 'rapid strike', true, $damage_done);
		$player->setSkill('orderus', 20, $maxTurns,'magic shield', false, $damage_done/2);
		// get skills and calculate damage
		$skills = $player->getSkills();
		$damageArray = $player->getDamageDoneBySkills($activePlayer->name, $inactivePlayer->name, $turns, $skills, $damage_done);
		$damageAttack = $damageArray['damage_attack'];
		$damageBlock = $damageArray['damage_def'];
		$messageActivePlayer = $damageArray['message']['activePlayer'];
		$messageInactivePlayer = $damageArray['message']['inactivePlayer'];
		$totalDamage = $damage_done + $damageAttack;
		// check defender luck to block the hit
		$hit = 'yes';
		if($player->isAttackBlocked($inactivePlayer, $turns, $maxTurns)){
			$damageBlock = $totalDamage;
			$hit = 'missed by luck';
		}
		// calculate defender health
		$defHealth = $inactivePlayer->health - ($totalDamage - $damageBlock);

		App::get('pdo')->beginTransaction();
		try{
			// store active player to turns table
			$turn->insert([
				'game_id' => $gameId,
				'player_id' => $activePlayer->player_id,
				'damage_done' => $totalDamage,
				'health_left' => $activePlayer->health,
				'hit' => 'no',
				'skills' => $messageActivePlayer,
			]);
			// store inactive player to turns table
			$turn->insert([
				'game_id' => $gameId,
				'player_id' => $inactivePlayer->player_id,
				'damage_done' => $damageBlock,
				'health_left' => $defHealth,
				'hit' => $hit,
				'skills' => $messageInactivePlayer
			]);

			// update column active in players table
			$player = new Player();
			$player->update(['active' => 1], 'id', $inactivePlayer->player_id);
			$player->update(['active' => 0], 'id', $activePlayer->player_id);

			App::get('pdo')->commit();
		}catch(Exception $exception){
			App::get('pdo')->rollback();
			throw $exception;
		}

		// set winner message
		if($activePlayer->health <= 0){
			$_SESSION['winner'] = 'Winner is: '.$inactivePlayer->name;
		}

		if($defHealth <= 0){
			$_SESSION['winner'] = 'Winner is: '.$activePlayer->name;
		}

		// set health left
		$_SESSION[$activePlayer->name] = $activePlayer->health;
		$_SESSION[$inactivePlayer->name] = $defHealth;

		return header("Location:{$_SERVER['HTTP_ORIGIN']}/start");
	}
}