<?php namespace App\Controllers;

use App\Models\Game;
use App\Models\Player;
use App\Models\Stat;
use App\Models\Turn;
use App\Services\GameService;
use Core\App;

class GameController{
	public function index(){
		session_destroy();
		return view('welcome.php');
	}

	public function store(){
			$game = new Game();
			$stat = new Stat();
			$playersStats = App::get('game')->getPlayersStats();

			App::get('pdo')->beginTransaction();
			try{
				$gameId = $game->insert();

				foreach($playersStats as $playerName => $playersStat){
					$player = new Player();
					$playerId = $player->where('name', $playerName)[0]->id;

					$stat->insert([
						'player_id' => $playerId,
						'game_id' => $gameId,
						'health' => $playersStat['health'],
						'strength' => $playersStat['strength'],
						'defence' => $playersStat['defence'],
						'speed' => $playersStat['speed'],
						'luck' => $playersStat['luck'],
					]);
				}

				App::get('pdo')->commit();
			}catch(\Exception $exception){
				App::get('pdo')->rollback();
				throw $exception;
			}

		return header("Location:{$_SERVER['HTTP_ORIGIN']}/start");
	}

	public function start() {
		$game = new Game();
		$player = new Player();

		if(!$game->isGameOn()) return header('Location: /');

		new GameService($game, $player);
		$gameId = $game->getCurrentGame()->id;
		$stats = $player->getPlayerStats($gameId);
		$turn = new Turn();
		$activePlayer = $player->getActivePlayer();
		$turns = $turn->rawSql("SELECT turns.id turn_id, turns.damage_done, turns.health_left, turns.hit, turns.skills, players.name 
									FROM turns INNER JOIN players ON turns.player_id = players.id  WHERE turns.game_id = $gameId ORDER BY turns.id ASC");

		return view( 'start-game.php', compact('stats', 'activePlayer', 'turns'));
	}

	public function end(){
		session_destroy();
		return header("Location: /");
	}

}