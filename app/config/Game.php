<?php namespace App\Config;

class Game{
	protected $maxTurns;
	protected $playersStats;

	public function setMaxTurns($max){
		$this->maxTurns = $max;
	}

	public function getMaxTurns(){
		return $this->maxTurns;
	}

	public function setPlayerStats($name, array $stats){
		$this->playersStats[$name] = $stats;
	}

	public function getPlayersStats(){
		return $this->playersStats;
	}
}