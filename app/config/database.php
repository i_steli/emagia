<?php

return 	[
	'mysql' => [
		'host' => 'localhost',
		'name' => 'emagia',
		'user' => 'root',
		'password' => '',
		'options' => [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		]
	]
];