<?php

use Core\Request;

function dd(...$params){
	var_dump($params);
	die();
}

function request($key = null){
	if($key === null){
		return new Request;
	}

	return $_REQUEST[$key];
}

function view($path, array $values = []){
	if(count($values) !== null){
		extract($values);
	};

	return require_once "views/".$path;
}