<?php namespace App\Models;

use Database\QueryBuilder;

class Turn  extends QueryBuilder{
	protected $table = 'turns';

	public function getTurns($gameId){
		$turns = $this->rawSql("SELECT * FROM turns WHERE turns.game_id = $gameId");
		return count($turns)/2;
	}
}