<?php namespace App\Models;

use Database\QueryBuilder;

class Game extends QueryBuilder{
	protected $table = 'games';

	public function getCurrentGame(){
		return $this->select('id', 'DESC')[0];
	}

	public function isGameOn(){
		return count($this->select('id', 'DESC')) != 0 ? true : false ;
	}
}