<?php namespace App\Models;

use Database\QueryBuilder;
use Exception;

class Player extends QueryBuilder{
	protected $table = 'players';
	protected $skills = [];
	protected $players = [];

	public function setRoles($playersStats, $gameId){
		// default active player
		$activePlayer = $playersStats[0];
		$inactivePlayer = $playersStats[1];

		// check turns and get the last one for attacker or defender
		$turn = new Turn();
		$turns = $turn->rawSql("SELECT * FROM turns WHERE game_id = $gameId LIMIT 1");
		if(count($turns)){
			$this->players['activePlayer'] = $this->getRolePlayer($gameId, 1);
			$this->players['inactivePlayer'] = $this->getRolePlayer($gameId, 0);
			return;
		}

		// set active player by speed or luck
		if($playersStats[0]->speed > $playersStats[1]->speed){
			$activePlayer = $playersStats[0];
			$inactivePlayer = $playersStats[1];
		}

		if($playersStats[0]->speed < $playersStats[1]->speed){
			$activePlayer = $playersStats[1];
			$inactivePlayer = $playersStats[0];
		}

		if($playersStats[0]->speed === $playersStats[1]->speed){
			$players = $this->checkLuckyStat($playersStats);
			$activePlayer = $players['activePlayer'];
			$inactivePlayer = $players['inactivePlayer'];
		}

		$this->players['activePlayer'] = $activePlayer;
		$this->players['inactivePlayer'] = $inactivePlayer;
	}

	public function checkLuckyStat($playersStats){
		if($playersStats[0]->luck > $playersStats[1]->luck){
			$activePlayer = $playersStats[0];
			$inactivePlayer = $playersStats[1];
		}

		if($playersStats[0]->luck < $playersStats[1]->luck){
			$activePlayer = $playersStats[1];
			$inactivePlayer = $playersStats[0];
		}

		return [
			'activePlayer' => $activePlayer,
			'inactivePlayer' => $inactivePlayer
		];
	}

	public function getPlayerStats($gameId){
		return $this->rawSql( "SELECT stats.game_id, players.id as player_id, stats.health, stats.strength, stats.defence, stats.speed, stats.luck, players.name, players.active
									  FROM stats 
									  INNER JOIN players ON stats.player_id = players.id 
									  WHERE stats.game_id = $gameId");
	}

	public function getRolePlayer(int $gameId,  int $active){
		return $this->rawSql("SELECT players.id as player_id, players.name, stats.strength, stats.defence, stats.luck, turns.health_left as health, turns.game_id
															 FROM players 
															 JOIN stats ON players.id = stats.player_id 
															 JOIN turns ON stats.player_id = turns.player_id 
															 WHERE players.active = $active AND stats.game_id = $gameId ORDER BY turns.id DESC LIMIT 1")[0];
	}

	public function getActivePlayer(){
		return $this->players['activePlayer'];
	}

	public function getInactivePlayer(){
		return $this->players['inactivePlayer'];
	}

	public function setSkill($playerName, $chancePercentage, $maxTurns, $skilllName, $attacker, $damageDone) : void{
		$damageDone = round($damageDone);

		if($chancePercentage == 0){
			throw new Exception("Do not add skill if the chance is 0");
		}

		$totalSkillTurns = ($chancePercentage*$maxTurns)/100;
		$turnPerSkill = round($maxTurns/$totalSkillTurns);

		$element = [
			'player_name' => $playerName,
			'percentage' => $chancePercentage,
			'total_skill_turns' => $totalSkillTurns,
			'turn_per_skill' => $turnPerSkill,
			'damage_done' => $damageDone,
			'attacker' => $attacker
		];

		$this->skills[$skilllName] = $element;
	}

	public function getSkills() : array{
		return $this->skills;
	}

	public function getDamageDoneBySkills($activePlayerName, $inactivePlayerName, $turns, $skills, $damageDone) : array{
		$damageAttack = 0;
		$damageDef = 0;
		$inactiveMessage = '';
		$activeMessage = '';

		foreach ($skills as $skillName => $skill){
			if($inactivePlayerName  == $skill['player_name'] && !$skill['attacker'] && ($turns+1)%$skill['turn_per_skill'] == 0){
				$damageDef += $skill['damage_done'];
				$inactiveMessage .= $skillName;
			}

			if($activePlayerName == $skill['player_name'] && $skill['attacker'] && ($turns+1)%$skill['turn_per_skill'] == 0){
				$damageAttack += $skill['damage_done'];
				$activeMessage .= $skillName;
			}
		}

		if($damageDef > $damageAttack + $damageDone){
			$damageDef = $damageAttack;
		}

		return [
			'damage_attack' => $damageAttack,
			'damage_def' => $damageDef,
			'message' => [
				'inactivePlayer' => $inactiveMessage,
				'activePlayer' => $activeMessage,
			]
		];
	}

	public function isAttackBlocked(Player $inactivePlayer, $turns, $maxTurns){
		$luckDefender = $inactivePlayer->luck;
		$totalLuckTurns = ($luckDefender*$maxTurns)/100;
		// division by zero
		if($totalLuckTurns == 0){
			return false;
		}

		$turnPerLuckTurns = round($maxTurns/$totalLuckTurns);
		if(($turns+1)%$turnPerLuckTurns == 0 ){
			return true;
		}

		return false;
	}
}