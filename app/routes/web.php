<?php
use Core\Router;

Router::get('/migrations', 'MigrationController@index');
Router::get('/', 'GameController@index');
Router::post('/store', 'GameController@store');
Router::get('/start', 'GameController@start');
Router::post('/end', 'GameController@end');
Router::post('/turns/store', 'TurnController@store');
Router::get('/test1/{name}/show/{id}', 'TestController@show');
Router::get('/test1/{id}', 'TestController@show2');
Router::get('/test1/{name}/{id}', 'TestController@show');
Router::get('/test1/show', 'TestController@show1');
Router::get('/test/{name}/show', 'TestController@test');
