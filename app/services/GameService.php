<?php namespace App\Services;

use App\Models\Game;
use App\Models\Player;

class GameService{
	public function __construct(Game $game, Player $player){
		$playersStats = $player->getPlayerStats($game->getCurrentGame()->id);
		$player->setRoles($playersStats, $game->getCurrentGame()->id);
	}
}