<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link type="text/css" rel="stylesheet" href="css/custom.css">
	<title>Welcome to Emagia</title>
</head>
<body>

<div class="wrapper">
	<?php foreach($stats as $key => $stat): ?>
		<div class="player-<?= $key ?>-panel <?= $activePlayer->name == $stat->name ? 'active' : '' ?>">
			<div class="player-stats">
				<ul>
					<li>Health <?= $stat->health ?></li>
					<li>Strength <?= $stat->strength ?></li>
					<li>Defence <?= $stat->defence ?></li>
					<li>Speed <?= $stat->speed ?></li>
					<li>Luck <?= $stat->luck ?></li>
				</ul>
			</div>
			<div class="player-name">
				<h3><?= ucfirst($stat->name) ?></h3>
			</div>
			<div class="player-image">
				<img src="<?= $key == 0 ? 'images/oderus.png' : 'images/beast.png' ?>" class="image-reponsive" alt="oderus">
			</div>
			<div class="player-live">
				<h4>Live <?= isset($_SESSION[$stat->name]) ? $_SESSION[$stat->name] : $stat->health ?></h4>
			</div>
		</div>
	<?php endforeach; ?>
	<form method="post" action="end" class="end-game">
		<button type="submit" class="btn-end">End Game</button>
	</form>
	<?php if(isset($_SESSION['winner'])): ?>
		<h4 class="attack">
			<span class="winner"><?= $_SESSION['winner'] ?></span>
		</h4>
	<?php elseif(isset($_SESSION['max_turns'])): ?>
		<h4 class="attack">
			<span class="winner"><?= $_SESSION['max_turns'] ?></span>
		</h4>
	<?php else: ?>
		<form method="post" action="turns/store" class="attack">
			<button type="submit" class="btn-end">Attack</button>
		</form>
	<?php endif; ?>
</div>

<div class="results">
	<h3 style="text-align: center">Turns <?= count($turns)/2 ?></h3>
	<table style="margin: 0 auto">
		<thead>
			<tr>
				<th>#</th>
				<th>Role</th>
				<th>Name</th>
				<th>Damage</th>
				<th>Health</th>
				<th>Hit</th>
				<th>Skills</th>
			</tr>
		</thead>

		<tbody style="text-align: center">
		<?php foreach($turns as $key => $turn): ?>
			<tr>
				<?php if(($key+1)%2 != 0): ?>
					<td rowspan="2"><?= round(($key+1)/2 ) ?></td>
				<?php endif; ?>
				<td style="text-align: center">
					<?= $turn->hit == 'no' ? 'Attack' : 'Deffend' ?>
				</td>
				<td  style="text-align: center">
					<?= ucfirst($turn->name) ?>
				</td  style="text-align: center">
				<td  style="text-align: center">
					<?= $turn->damage_done ?>
				</td>
				<td  style="text-align: center">
					<?= $turn->health_left ?>
				</td>
				<td  style="text-align: center">
					<?= $turn->hit ?>
				</td>
				<td  style="text-align: center">
					<?= $turn->skills == '' ? 'none' : $turn->skills ?>
				</td>
			</tr>
			<?php if(($key+1)%2 == 0): ?>
				<tr>
					<td colspan="7"><hr></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>



</body>
</html>