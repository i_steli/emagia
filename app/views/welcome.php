<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link type="text/css" rel="stylesheet" href="css/custom.css">
	<title>Welcome to Emagia</title>
</head>
<body>

<div class="wrapper">
	<div class="player-0-panel">
		<div class="player-stats"></div>
		<div class="player-name">
			<h3>Orderus</h3>
		</div>
		<div class="player-image">
			<img src="images/oderus.png" class="image-reponsive" alt="orderus">
		</div>
	</div>

	<div class="player-1-panel">
		<div class="player-stats"></div>
		<div class="player-name">
			<h3>Beast</h3>
		</div>
		<div class="player-image">
			<img src="images/beast.png" class="image-reponsive" alt="beast">
		</div>
	</div>

	<form method="post" action="store" class="new-game">
		<input type="submit" class="btn-start" value="Start Game">
	</form>
</div>



</body>
</html>