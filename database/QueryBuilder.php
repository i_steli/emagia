<?php namespace Database;

use Core\App;
use PDO;

class QueryBuilder{
	private $pdo;

	public function __construct() {
		$this->pdo = App::get('pdo');
	}

	public function insert(array $values = []) : int{
		$cols = implode(', ', array_keys($values));
		$placeholders = preg_replace("/(\w+)/", ":$1", $cols);
		$sql = "INSERT INTO $this->table ($cols) VALUES ($placeholders)";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute($values);
		return $this->pdo->lastInsertId();
	}

	public function select($orderCol = 'created_at', $sort = 'ASC'){
		$sql = "SELECT * FROM $this->table ORDER BY $orderCol $sort";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_CLASS, get_class($this));
	}

	public function where($col, $value){
		$sql = "SELECT * FROM $this->table where $col=:$col";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute([$col => $value]);
		return $stmt->fetchAll(PDO::FETCH_CLASS, get_class($this));
	}

	public function rawSql($sql){
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_CLASS, get_class($this));
	}

	public function update(array $values, $col, $colValue, $operator = "="){
		$cols = '';
		foreach($values  as $key => $value){
			$cols .= $key .'=:'.$key.',';
		}
		$cols = trim($cols, ',');
		$sql = "UPDATE $this->table SET $cols WHERE $col $operator $colValue";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute($values);
	}
}