<?php namespace Database;

use PDO;

class Connection{
	public static function make(array $config){
		try{
			return new PDO(
				"mysql:host=".$config["host"].";dbname=".$config['name'],
				$config['user'],
				$config['password'],
				$config['options']
			);
		}catch(\PDOException $exception){
			die($exception->getMessage());
		}
	}
}